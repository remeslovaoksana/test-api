import { ApiRequest } from "../request";

const baseUrl = global.appConfig.baseUrl;

export type RegistrationSchema = {
    avatar: string;
    email: string;
    userName: string;
    password: string;
};
export type LoginSchema = {
    email: string;
    password: string;
};

export class AuthController {
    async login(body: LoginSchema) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`Auth/login`)
            .body(body)
            .send();
        return response;
    }

    async registration(body: RegistrationSchema) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`Register`)
            .body(body)
            .send();
        return response;
    }
}
