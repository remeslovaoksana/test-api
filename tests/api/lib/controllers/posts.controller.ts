import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export type NewPostSchema = {
    authorId: number;
    previewImage: string;
    body: string;
};
export type CommentPostSchema = {
    authorId: number;
    postId: number;
    body: string;
};
export type LikePostSchema = {
    entityId: number;
    isLike: boolean;
    userId: number;
};

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`Posts`).send();
        return response;
    }

    async newPost(body: NewPostSchema, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`Posts`)
            .body(body)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addPostComment(body: CommentPostSchema, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`Comments`)
            .body(body)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async likePost(body: LikePostSchema, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`Posts/like`)
            .body(body)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
