import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export type UpdateUserSchema = {
    id: number;
    avatar: string;
    email: string;
    userName: string;
}
export type UserSchema = {
    id: string;
}

export class UsersController {
    async getAllUsers() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`Users`)
            .send();
        return response;
    }

    async getUserById(id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`Users/${id}`)
            .send();
        return response;
    }

    async getCurrentUser(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
   
    async updateUser(userData: UpdateUserSchema, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("PUT")
            .url(`Users`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async deleteUser(id: number,  accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("DELETE")
            .url(`Users/${id}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
 }